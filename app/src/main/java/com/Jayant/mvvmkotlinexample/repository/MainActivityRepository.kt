package com.Jayant.mvvmkotlinexample.repository

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.Jayant.mvvmkotlinexample.model.fetchImagesData.DataResponse
import com.Jayant.mvvmkotlinexample.retrofit.RetrofitClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

object MainActivityRepository {

    val imagesMutableLiveData = MutableLiveData<DataResponse>()

    fun getServicesApiCall(): MutableLiveData<DataResponse> {

        fun getHeadersAPI1(): Map<String, String> {
            val headers = HashMap<String, String>()
            return headers;
        }

        val call = RetrofitClient.apiInterface.getHealthMEtrics(getHeadersAPI1())

        call.enqueue(object: Callback<DataResponse> {
            override fun onFailure(call: Call<DataResponse>, t: Throwable) {
                // TODO("Not yet implemented")
                Log.v("DEBUG : ", t.message.toString())
            }

            override fun onResponse(
                call: Call<DataResponse>,
                response: Response<DataResponse>
            ) {
                // TODO("Not yet implemented")
                Log.v("DEBUG : ", response.body().toString())

                val data = response.body()

                imagesMutableLiveData.value = response.body()
            }
        })

        return imagesMutableLiveData
    }
}