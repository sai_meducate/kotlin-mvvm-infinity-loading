package com.Jayant.mvvmkotlinexample.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.Jayant.mvvmkotlinexample.repository.MainActivityRepository
import com.Jayant.mvvmkotlinexample.model.fetchImagesData.DataResponse

class MainActivityViewModel : ViewModel() {

    var servicesLiveData: MutableLiveData<DataResponse>? = null

    fun getUser() : LiveData<DataResponse>? {
        servicesLiveData = MainActivityRepository.getServicesApiCall()
        return servicesLiveData
    }

}

