package com.Jayant.mvvmkotlinexample.view

import android.content.Context
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.Jayant.mvvmkotlinexample.R
import com.Jayant.mvvmkotlinexample.model.fetchImagesData.DataResponse
import com.Jayant.mvvmkotlinexample.model.fetchImagesData.MemesObj
import com.Jayant.mvvmkotlinexample.viewmodel.MainActivityViewModel
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    private var totalObjects: Int? = 0
    private var recyclerView: RecyclerView? = null
    private var adapter: ImagesAdapter? = null
    private var memesObjList: ArrayList<MemesObj>? = null
    lateinit var context: Context

    lateinit var mainActivityViewModel: MainActivityViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        context = this@MainActivity

        initViewModel();
        initViews();
        observerObjectForImage()
    }

    private fun observerObjectForImage() {
        mainActivityViewModel.getUser()!!.observe(this, Observer { serviceSetterGetter ->
            wp7progressBar.hideProgressBar()
            val parentdata: DataResponse = serviceSetterGetter
            memesObjList!!.addAll(parentdata.data.memes)
            adapter!!.notifyDataSetChanged();
            totalObjects=memesObjList!!.size
        })
    }

    private fun initViewModel() {
        mainActivityViewModel = ViewModelProvider(this).get(MainActivityViewModel::class.java)
    }

    private fun initViews() {
        wp7progressBar.showProgressBar()
        memesObjList = ArrayList<MemesObj>()
        recyclerView = findViewById<RecyclerView>(R.id.recycler_view)
        adapter = ImagesAdapter(memesObjList!!)
        val layoutManager = StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL)
        layoutManager.gapStrategy = StaggeredGridLayoutManager.GAP_HANDLING_NONE
        recyclerView!!.setLayoutManager(layoutManager)
        recyclerView!!.setHasFixedSize(true)
        recyclerView!!.setItemViewCacheSize(40);
        recyclerView!!.setDrawingCacheEnabled(true);
        recyclerView!!.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        recyclerView!!.adapter = adapter

        if(totalObjects!!>4) {
            totalObjects = totalObjects!! - 4
        }

        recyclerView!!.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (dy > totalObjects!!) { //check for scroll down
                    mainActivityViewModel.getUser()
                }
            }
        })
    }
}
