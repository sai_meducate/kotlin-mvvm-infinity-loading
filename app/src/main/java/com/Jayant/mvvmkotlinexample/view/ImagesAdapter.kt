package com.Jayant.mvvmkotlinexample.view

import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.Jayant.mvvmkotlinexample.R
import com.Jayant.mvvmkotlinexample.model.fetchImagesData.MemesObj
import com.Jayant.mvvmkotlinexample.view.ImagesAdapter.NotifiView
import com.squareup.picasso.Picasso
import java.util.*

class ImagesAdapter(var addNotiModel: ArrayList<MemesObj>) :
    RecyclerView.Adapter<NotifiView>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NotifiView {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.list_card_images, parent, false)
        return NotifiView(view)
    }

    override fun onBindViewHolder(holder: NotifiView, position: Int) {
        if (!TextUtils.isEmpty(addNotiModel[position].url)) {
            Picasso.get().load(addNotiModel[position].url).into(holder.productImage)
        }
        holder.imgName.text = addNotiModel[position].name
        holder.imgDesc.text = addNotiModel[position].id
    }

    override fun getItemCount(): Int {
        return addNotiModel.size
    }

    inner class NotifiView(itemView: View) : ViewHolder(itemView) {
        var productImage: ImageView
        var imgName: TextView
        var imgDesc: TextView

        init {
            productImage = itemView.findViewById(R.id.productImage)
            imgName = itemView.findViewById(R.id.imgName)
            imgDesc = itemView.findViewById(R.id.imgDesc)
        }
    }
}