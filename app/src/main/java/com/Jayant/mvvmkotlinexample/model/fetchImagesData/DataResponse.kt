package com.Jayant.mvvmkotlinexample.model.fetchImagesData

import java.io.Serializable

data class DataResponse (val success:Boolean,
                         val data: PayoutData
): Serializable