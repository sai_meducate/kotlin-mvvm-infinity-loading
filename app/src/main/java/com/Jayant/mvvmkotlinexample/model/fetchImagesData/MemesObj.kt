package com.Jayant.mvvmkotlinexample.model.fetchImagesData

import java.io.Serializable

data class MemesObj(
    val id: String,
    val name: String,
    val url: String,
    val width: Int,
    val height: Int,
    val box_count: Int
) : Serializable