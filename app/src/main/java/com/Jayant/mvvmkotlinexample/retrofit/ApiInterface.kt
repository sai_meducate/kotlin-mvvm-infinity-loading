package com.Jayant.mvvmkotlinexample.retrofit

import com.Jayant.mvvmkotlinexample.model.fetchImagesData.DataResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.HeaderMap

interface ApiInterface {

    @GET("/get_memes")
    fun getHealthMEtrics(@HeaderMap headers: Map<String, String>): Call<DataResponse>


}